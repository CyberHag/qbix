﻿using InterfaceObjectStorage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace qbix
{
    public partial class PeopleList : Form
    {
        public PeopleList()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Обрабатываем событие загрузки формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PeopleList_Load(object sender, EventArgs e)
        {
            dataGridPeople.AutoGenerateColumns = false;
            UpdateData();
        }
        /// <summary>
        /// Метод асинхронного запроса данных о сотрудниках
        /// </summary>
        public void UpdateData()
        {
            ThreadPool.QueueUserWorkItem(UpdateDataForPool);
        }
        /// <summary>
        /// Запрос данных о сотрудниках и выгрузка в грид
        /// </summary>
        /// <param name="value"></param>
        private void UpdateDataForPool(Object value)
        {
            string error = string.Empty;
            List<IPeople> peoples = Program.Storage.LoadEnumerablePeople(out error).ToList();
            BindingSource source = new BindingSource();
            source.DataSource = peoples;
            this.BeginInvoke((Action)(() =>
            {
                dataGridPeople.DataSource = source;
            }));

        }
        /// <summary>
        /// Корректно выводим в ячейку грида сведения о должности
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridPeople_CellFormatting(object sender, System.Windows.Forms.DataGridViewCellFormattingEventArgs e)
        {
            if (e != null && e.Value != null && dataGridPeople.Columns[e.ColumnIndex].Name == "ColumnPosition")
            {
                e.Value = (e.Value as IPosition).Name;
            }
        }
        /// <summary>
        /// Обрабатываем событие двойного клика - открывая форму сотрудника на редактирование
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridPeople_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            IPeople people = ((dataGridPeople.DataSource as BindingSource).Current as IPeople);
            if (people != null)
            {
                string name = string.Concat("People ", people.ID);
                // Нельзя открывать на одного сотрудника одновременно несколько форм, но можно открыть несколько форм на разных сотрудников
                Form form = MdiParent.MdiChildren.SingleOrDefault(f => f.Name.Equals(name));
                if (form == null)
                {
                    form = new PeopleEdit(people);
                    form.MdiParent = this.MdiParent;
                    form.Name = name;
                    form.Show();
                    form.WindowState = FormWindowState.Maximized;
                }
                else
                {
                    form.Show();
                }

            }
        }
        /// <summary>
        /// Открываем форму для добавления нового сотрудника
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form form = new PeopleEdit(Program.Storage.GetEmptyPeople());
            form.MdiParent = this.MdiParent;
            form.Name = "People 0";
            form.Show();
            form.WindowState = FormWindowState.Maximized;
        }
    }
}
