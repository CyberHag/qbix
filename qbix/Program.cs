﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace qbix
{
    static class Program
    {
        private static InterfaceObjectStorage.IStorage storage = null;
        public static InterfaceObjectStorage.IStorage Storage { get { return storage; } }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string libraryStoragePath = ConfigurationManager.AppSettings["LibraryStoragePath"];
            storage = GetStorage(libraryStoragePath);
            // Если не удалось загрузить библиотеку работы с хранилищем, то в работе приложения нет смысла
            if (storage == null)
            {
                throw new Exception("Can't load library for connection to storagy");
            }
            else
            {
                storage.ConnectionString = ConfigurationManager.ConnectionStrings["Main"].ConnectionString;

                Application.Run(new MDIMain());
            }
        }
        /// <summary>
        /// Динамическая загрузка библиотеки взаимодействия с хранилищем данных
        /// </summary>
        /// <param name="LibraryStoragePath">Путь и имя файла библиотеки</param>
        /// <returns>Экземпляр объекта взаимодействия с хранилищем</returns>
        static InterfaceObjectStorage.IStorage GetStorage(string LibraryStoragePath)
        {
            InterfaceObjectStorage.IStorage storage = null;
            try
            {
                Assembly assembly = Assembly.LoadFrom(LibraryStoragePath);
                Type typeStorage = null;
                foreach (Type oneType in assembly.GetTypes())
                {
                    if (oneType.GetInterface(typeof(InterfaceObjectStorage.IStorage).FullName) != null)
                    {
                        typeStorage = oneType;
                        break;
                    }
                }
                if (typeStorage != null)
                {
                    storage = Activator.CreateInstance(typeStorage) as InterfaceObjectStorage.IStorage;
                }
            }
            catch 
            {
                storage = null;
            }
            return storage;
        }
    }
}
