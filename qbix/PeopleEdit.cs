﻿using InterfaceObjectStorage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace qbix
{
    public partial class PeopleEdit : Form
    {
        /// <summary>
        /// Информация о сотруднике
        /// </summary>
        protected IPeople People { get; set; }
        /// <summary>
        /// Скрываем конструктор по умолчанию, чтобы всегда работать с существующим объектом сотрудника
        /// </summary>
        private PeopleEdit()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Конструктор получающий объект сотрудника
        /// </summary>
        /// <param name="People">Сотрудник</param>
        public PeopleEdit(IPeople People)
        {
            InitializeComponent();
            this.People = People;
        }
        /// <summary>
        /// Метод асинхронного запроса данных о списке должностей
        /// </summary>
        protected void UpdatePositionList()
        {
            ThreadPool.QueueUserWorkItem(UpdatePositionListForPool);
        }
        /// <summary>
        /// Метод асинхронного запроса данных о сотруднике
        /// </summary>
        protected void UpdatePeople()
        {
            ButtonSetEnable(false);
            ThreadPool.QueueUserWorkItem(UpdatePeopleForPool);
        }
        /// <summary>
        /// Запрос данных о сотруднике и выгрузка результата в UI
        /// </summary>
        /// <param name="value"></param>
        protected void UpdatePeopleForPool(Object value)
        {
            string error = string.Empty;
            if (People.Update(out error) && string.IsNullOrEmpty(error))
            {
                this.BeginInvoke((Action)(() =>
                {
                    txtName.Text = People.Name;
                    cmbPosition.SelectedValue = People.Positions.ID;
                    ButtonSetEnable(true);
                }));
            }
            else if (!string.IsNullOrEmpty(error))
            {
                this.BeginInvoke((Action)(() => { MessageBox.Show(error); }));
            }
            else
            {
                this.BeginInvoke((Action)(() => { MessageBox.Show("Can't load people"); }));
            }
        }
        /// <summary>
        /// Запрос списка должностей и выгрузка данных в UI
        /// </summary>
        /// <param name="value"></param>
        protected void UpdatePositionListForPool(Object value)
        {
            string error = string.Empty;
            List<IPosition> positions = Program.Storage.LoadEnumerablePosition(out error).ToList();
            if (string.IsNullOrEmpty(error))
            {
                BindingSource bindingSource = new BindingSource();
                bindingSource.DataSource = positions;
                this.BeginInvoke((Action)(() =>
                {
                    cmbPosition.DataSource = bindingSource;
                    if (People.Positions != null)
                    {
                        cmbPosition.SelectedValue = People.Positions.ID;
                    }
                }));
            }
            else
            {
                this.BeginInvoke((Action)(() =>
                {
                    MessageBox.Show(error);
                }));
            }
        }
        /// <summary>
        /// Обработка события загрузки формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PeopleEdit_Load(object sender, EventArgs e)
        {
            UpdatePositionList();
            if (People.ID > 0)
            {
                UpdatePeople();
            }
            else
            {
                ButtonSetEnable(true);
            }
        }
        /// <summary>
        /// Сохраняем данные и закрываем форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (LoadPeopleFromUI())
            {
                ButtonSetEnable(false);
                SavePeople();
            }
        }
        /// <summary>
        /// Метод асинхронного сохранения данных о сотруднике
        /// </summary>
        private void SavePeople()
        {
            ThreadPool.QueueUserWorkItem(SavePeopleForPool);
        }
        /// <summary>
        /// Метод сохранения данных о сотруднике, закрытии формы, и обновления данных в списке
        /// </summary>
        /// <param name="value"></param>
        private void SavePeopleForPool(object value)
        {
            string error = string.Empty;
            if (People.Save(out error) && string.IsNullOrEmpty(error))
            {
                PeopleList peopleList = MdiParent.MdiChildren.SingleOrDefault(f => f.Name == MDIMain.namePeopleList) as PeopleList;
                peopleList?.UpdateData();
                this.BeginInvoke((Action)(() =>
                {
                    this.Close();
                }));
            }
            else
            {
                if (string.IsNullOrEmpty(error))
                {
                    error = "Can't save people";
                }
                this.BeginInvoke((Action)(() =>
                {
                    MessageBox.Show(error);
                    ButtonSetEnable(true);
                }));
            }
        }
        /// <summary>
        /// Закрываем форму ничего не меняя в данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Уточняем необходимость удаления данных о сотруднике
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ButtonSetEnable(false);
                DeletePeople();
            }
        }
        /// <summary>
        /// Асинхронное удаление данных о сотруднике
        /// </summary>
        private void DeletePeople()
        {
            ThreadPool.QueueUserWorkItem(DeletePeopleForPool);
        }
        /// <summary>
        /// Удаление данных о сотруднике, закрытие формы и обновление данных в форме списка сотрудников
        /// </summary>
        /// <param name="value"></param>
        private void DeletePeopleForPool(Object value)
        {
            string error = string.Empty;
            if (People.Delete(out error) && string.IsNullOrEmpty(error))
            {
                PeopleList peopleList = (MdiParent.MdiChildren.SingleOrDefault(f => f.Name == MDIMain.namePeopleList) as PeopleList);
                peopleList?.UpdateData();
                this.BeginInvoke((Action)(() => { this.Close(); }));
            }
            else
            {
                if (string.IsNullOrEmpty(error))
                {
                    error = "Can't delete people";
                }
                this.BeginInvoke((Action)(() =>
                {
                    MessageBox.Show(error);
                    ButtonSetEnable(true);
                }));
            }
        }
        /// <summary>
        /// Установка доступности кнопок на UI
        /// </summary>
        /// <param name="SetEnable"></param>
        private void ButtonSetEnable(bool SetEnable)
        {
            btnSave.Enabled = SetEnable;
            btnCancel.Enabled = SetEnable;
            btnDelete.Enabled = SetEnable;
        }
        /// <summary>
        /// Перегрузка обновленных данных из UI в объект сотрудника
        /// </summary>
        /// <returns></returns>
        private bool LoadPeopleFromUI()
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                MessageBox.Show("Name can't be empty");
                return false;
            }
            else
            {
                People.Name = txtName.Text.Trim();
                People.Positions.ID = (cmbPosition.SelectedValue as int?) ?? 0;
                People.Skills = new List<ISkillLevel>();
                foreach(DataGridViewRow row in dataGridLevel.Rows)
                {
                    ISkillLevel level = Program.Storage.GetEmptySkillLevel();
                    level.ID = (int)((row.Cells[ColumnID.Name].Value as int?)??0);
                    level.Level = Int32.Parse((row.Cells[ColumnLevel.Name].Value as string) ?? "0");
                    (People.Skills as List<ISkillLevel>).Add(level);
                }
                return true;
            }
        }
        /// <summary>
        /// Обрабатываем смены должности
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateSkill();
        }
        /// <summary>
        /// Метод асинхронной загрузки навыков для выбранной должности
        /// </summary>
        private void UpdateSkill()
        {
            dataGridLevel.DataSource = null;
            int? positionID = cmbPosition.SelectedValue as int?;
            if (positionID != null && positionID != 0)
            {
                ThreadPool.QueueUserWorkItem(UpdateSkillForPool, positionID);
            }
        }
        /// <summary>
        /// Метод загрузки навыков и отображения их в гриде
        /// </summary>
        /// <param name="value"></param>
        private void UpdateSkillForPool(object value)
        {
            string error = string.Empty;
            IPosition position = Program.Storage.LoadPosition((int)value, out error);
            if (position != null && position.Skills != null && string.IsNullOrEmpty(error))
            {
                BindingSource bindingSource = new BindingSource();
                bindingSource.DataSource = position.Skills;
                this.BeginInvoke((Action)(() =>
                {
                    if ((cmbPosition.SelectedValue as int?) == (int)value)
                    {
                        dataGridLevel.AutoGenerateColumns = false;
                        dataGridLevel.DataSource = bindingSource;
                        if (People.Positions.ID == (int)value)
                        {
                            foreach(DataGridViewRow row in dataGridLevel.Rows)
                            {
                                row.Cells[ColumnLevel.Name].Value = People.Skills.SingleOrDefault(l => l.ID == ((int)row.Cells[ColumnID.Name].Value)).Level.ToString();
                            }
                        }
                    }
                }));
            }
        }

        private void dataGridLevel_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
