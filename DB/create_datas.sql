﻿USE [qbix]
GO

INSERT INTO [dbo].[skill] ([name]) VALUES ('c++')
INSERT INTO [dbo].[skill] ([name]) VALUES ('c#')
INSERT INTO [dbo].[skill] ([name]) VALUES ('Переговоры')
GO

USE [qbix]
GO

INSERT INTO [dbo].[position] ([name]) VALUES ('Программист')
INSERT INTO [dbo].[position] ([name]) VALUES ('Директор')
GO

USE [qbix]
GO

INSERT INTO [dbo].[position_skill] ([skill_id] ,[position_id]) VALUES (1 ,1)
INSERT INTO [dbo].[position_skill] ([skill_id] ,[position_id]) VALUES (2 ,1)
INSERT INTO [dbo].[position_skill] ([skill_id] ,[position_id]) VALUES (3 ,3)
GO

USE [qbix]
GO

INSERT INTO [dbo].[people] ([name] ,[position_id]) VALUES ('Михаил' ,1)
INSERT INTO [dbo].[people] ([name] ,[position_id]) VALUES ('Дмитрий' ,1)
INSERT INTO [dbo].[people] ([name] ,[position_id]) VALUES ('Елена' ,2)
GO



