﻿using InterfaceObjectStorage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Storage;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitStorage
{
    [TestClass]
    public class UPosition
    {
        [TestMethod]
        public void TestAddandDelete()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            string error = string.Empty;
            int id_1 = 0;
            IPosition position_new = new PositionR(storage)
            {
                Name = string.Concat("Test_Add_Position", DateTime.Now.ToShortTimeString()),
                Skills = new List<ISkill>()
            };
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select max([id]) from [position]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id_1 = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                using (SqlCommand command = new SqlCommand("select top 3 * from [skill] order by [id] desc", connection))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ISkill skill = new SkillR(storage)
                        {
                            ID = reader.GetInt32(reader.GetOrdinal("id")),
                            Name = reader.GetString(reader.GetOrdinal("name"))
                        };
                        (position_new.Skills as List<ISkill>).Add(skill);
                    }
                    reader.Close();
                }
            }
            if (position_new.Save(out error))
            {
                int id_2 = 0;
                int pos_skill_count = 0;
                using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
                {
                    using (SqlCommand command = new SqlCommand("select max([id]) from [position]", connection))
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            id_2 = reader.GetInt32(0);
                        }
                        reader.Close();
                    }
                    using(SqlCommand command = new SqlCommand(string.Concat("select count(1) from [position_skill] where [position_id] = ", position_new.ID), connection))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            pos_skill_count = reader.GetInt32(0);
                        }
                        reader.Close();
                    }
                }
                if (id_1 != id_2 && pos_skill_count == position_new.Skills.Count())
                {
                    IPosition position = storage.LoadPosition(position_new.ID, out error);
                    bool result = position.Delete(out error);
                    int id_3 = 0;
                    using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
                    {
                        using (SqlCommand command = new SqlCommand("select max([id]) from [position]", connection))
                        {
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.Read())
                            {
                                id_3 = reader.GetInt32(0);
                            }
                            reader.Close();
                        }
                        using(SqlCommand command = new SqlCommand(string.Concat("select count(1) from [position_skill] where [position_id] = ", position_new.ID), connection))
                        {
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.Read())
                            {
                                pos_skill_count = reader.GetInt32(0);
                            }
                            reader.Close();
                        }
                    }
                    Assert.IsTrue(position != null && string.IsNullOrEmpty(error) && position != null && result && id_3 != position.ID && id_3 != id_2 && pos_skill_count == 0);
                }
                else
                {
                    Assert.Fail("Can't add new position");
                }
            }
            else
            {
                Assert.Fail(string.Concat("Can't add new position ", error));
            }
        }

        [TestMethod]
        public void TestUpdate()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            int id = 0;
            string error = string.Empty;
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select max([id]) from [position]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id = reader.GetInt32(0);
                    }
                }
            }
            IPosition position = storage.LoadPosition(id, out error);
            if (position != null && string.IsNullOrEmpty(error))
            {
                string name_true = position.Name;
                int skill_count = position.Skills.Count();
                position.Name = string.Concat(position.Name, DateTime.Now.ToShortTimeString());
                (position.Skills as List<ISkill>).Clear();
                Assert.IsTrue(position.Update(out error) && string.IsNullOrEmpty(error) && position.Name.Equals(name_true) && position.Skills.Count() == skill_count);
            }
            else
            {
                Assert.Fail(string.Concat("Can't load position", error));
            }
        }
        [TestMethod]
        public void TestSave()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            int id = 0;
            string error = string.Empty;
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select max([id]) from [position]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id = reader.GetInt32(0);
                    }
                }
            }
            IPosition position = storage.LoadPosition(id, out error);
            if (position != null)
            {
                string name = position.Name;
                string name_date = DateTime.Now.ToShortTimeString();
                if (name.Length > name_date.Length + 1)
                {
                    position.Name = string.Concat(name.Substring(0, name.Length - name_date.Length - 1), " ", name_date);
                }
                else
                {
                    position.Name = string.Concat(name, " ", name_date);
                }
                if (position.Skills.Count() > 0)
                {
                    position.Skills = position.Skills.Where(s => s.ID != position.Skills.First().ID).ToList<ISkill>();
                }
                if (position.Save(out error) && string.IsNullOrEmpty(error))
                {
                    using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
                    {
                        string name_new = string.Empty;
                        using (SqlCommand command = new SqlCommand(string.Concat("select * from [position] where [id] = ", id), connection))
                        {
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.Read())
                            {
                                name_new = reader.GetString(reader.GetOrdinal("name"));
                            }
                            reader.Close();
                        }
                        using(SqlCommand command = new SqlCommand(string.Concat("select count(1) from [position_skill] where [position_id] = ", id), connection))
                        {
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.Read())
                            {
                                Assert.IsTrue(position.Name.Equals(name_new) && reader.GetInt32(0) == position.Skills.Count());
                            }
                            else
                            {
                                Assert.Fail("Can't load position");
                            }
                        }
                    }
                }
                else
                {
                    Assert.Fail(error);
                }
            }
            else
            {
                Assert.Fail("Can't load position");
            }
        }
    }
}
