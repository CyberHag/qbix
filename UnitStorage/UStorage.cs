﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using InterfaceObjectStorage;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitStorage
{
    [TestClass]
    public class UStorage
    {
        [TestMethod]
        public void TestLoadEnumerableSkill()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            string Error = string.Empty;
            int? count = null;
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select count(1) from [skill]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        count = reader.GetInt32(0);
                    }
                }
            }
            IEnumerable<ISkill> enumerable = storage.LoadEnumerableSkill(out Error);
            Assert.IsTrue(enumerable != null && String.IsNullOrEmpty(Error) && enumerable.Count() == count);
        }
        [TestMethod]
        public void TestLoadEnumerablePosition()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            int? count = null;
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select count(1) from [position]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        count = reader.GetInt32(0);
                    }
                }
            }
            string Error = string.Empty;
            IEnumerable<IPosition> enumerable = storage.LoadEnumerablePosition(out Error);
            Assert.IsTrue(enumerable != null && String.IsNullOrEmpty(Error) && (from IPosition item in enumerable select item).Count() == count);
        }
        [TestMethod]
        public void TestLoadEnumerablePeople()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            int? count = null;
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select count(1) from [people]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        count = reader.GetInt32(0);
                    }
                }
            }
            string Error = string.Empty;
            IEnumerable<IPeople> enumerable = storage.LoadEnumerablePeople(out Error);
            Assert.IsTrue(enumerable != null && String.IsNullOrEmpty(Error) && (from IPeople item in enumerable select item).Count() == count);
        }
        [TestMethod]
        public void TestLoadPeople()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();
            string error = string.Empty;

            int id = 0;
            string name = string.Empty;
            using(SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using(SqlCommand command = new SqlCommand("", connection))
                {
                    command.CommandText = @"select * from [people] where [id] in (select max(id) from [people])";
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id = reader.GetInt32(reader.GetOrdinal("id"));
                        name = reader.GetString(reader.GetOrdinal("name"));
                    }
                }
            }
            var people = storage.LoadPeople(id, out error);
            Assert.IsTrue(string.IsNullOrEmpty(error) && people != null && people.Name.Equals(name) && people.Positions.Skills.Count() == people.Skills.Count());
        }
        [TestMethod]
        public void TestLoadPosition()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();
            string error = string.Empty;

            int id = 0;
            string name = string.Empty;
            using(SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using(SqlCommand command = new SqlCommand("select * from [position] where [id] in (select max([id]) from [position])", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id = reader.GetInt32(reader.GetOrdinal("id"));
                        name = reader.GetString(reader.GetOrdinal("name"));
                    }
                }
            }
            var position = storage.LoadPosition(id, out error);
            Assert.IsTrue(string.IsNullOrEmpty(error) && position != null && position.Name.Equals(name));
        }
        [TestMethod]
        public void TestLoadSkill()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            string error = string.Empty;
            int id = 0;
            string name = string.Empty; 
            using(SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using(SqlCommand command = new SqlCommand("select * from [skill] where [id] in (select max([id]) from [skill])", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id = reader.GetInt32(reader.GetOrdinal("id"));
                        name = reader.GetString(reader.GetOrdinal("name"));
                    }
                }
            }
            ISkill skill = storage.LoadSkill(id, out error);
            Assert.IsTrue(string.IsNullOrEmpty(error) && skill != null && skill.Name.Equals(name));
        }
    }
}
