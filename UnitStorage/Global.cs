﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitStorage
{
    class Global
    {
        static string connectionString = String.Empty;

        public static string GetConnectionString()
        {
            if (string.IsNullOrEmpty( connectionString))
            {
                connectionString = ConfigurationManager.ConnectionStrings["Main"].ConnectionString;
            }
            return connectionString;
        }
    }
}
