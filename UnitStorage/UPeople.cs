﻿using InterfaceObjectStorage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Storage;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitStorage
{
    [TestClass]
    public class UPeople
    {
        [TestMethod]
        public void TestUpdate()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            int id = 0;
            string error = string.Empty;
            using(SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using(SqlCommand command = new SqlCommand("select max([id]) from [people]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id = reader.GetInt32(0);
                    }
                }
            }
            IPeople people = storage.LoadPeople(id, out error);
            if (people != null) {
                string name = people.Name;
                string position = people.Positions.Name;
                int lev_count = people.Skills.Count();

                people.Name = DateTime.Now.ToShortTimeString();
                people.Positions.Name = "none";
                people.Skills = new List<ISkillLevel>();

                Assert.IsTrue(people.Update(out error) && string.IsNullOrEmpty(error) && people.Name.Equals(name) && people.Positions.Name.Equals(position) && people.Skills.Count() == lev_count);
            }
            else
            {
                Assert.Fail("Can't load people");
            }
        }
        [TestMethod]
        public void TestAddandDelete()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            string error = string.Empty;
            int id_1 = 0;
            IPeople people_new = new PeopleR(storage)
            {
                Name = string.Concat("Test_Add_Position", DateTime.Now.ToShortTimeString()),
                Positions = new PositionR(storage),
                Skills = new List<ISkillLevel>()
            };
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select max([id]) from [people]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id_1 = reader.GetInt32(0);
                    }
                    reader.Close();
                }
                using (SqlCommand command = new SqlCommand("select max([id]) from [position] ", connection))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        people_new.Positions = storage.LoadPosition(reader.GetInt32(0), out error);
                        foreach(ISkill skill in people_new.Positions.Skills)
                        {
                            ISkillLevel level = new SkillLevelR(storage)
                            {
                                ID = skill.ID,
                                Name = skill.Name,
                                Level = 0
                            };
                            (people_new.Skills as List<ISkillLevel>).Add(level);
                        }
                    }
                    reader.Close();
                }
            }
            if (people_new.Save(out error))
            {
                int id_2 = 0;
                int pos_skill_count = 0;
                using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
                {
                    using (SqlCommand command = new SqlCommand("select max([id]) from [people]", connection))
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            id_2 = reader.GetInt32(0);
                        }
                        reader.Close();
                    }
                }
                if (id_1 != id_2)
                {
                    IPeople people = storage.LoadPeople(people_new.ID, out error);
                    bool result = people.Delete(out error);
                    int id_3 = 0;
                    using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
                    {
                        using (SqlCommand command = new SqlCommand("select max([id]) from [people]", connection))
                        {
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.Read())
                            {
                                id_3 = reader.GetInt32(0);
                            }
                            reader.Close();
                        }
                    }
                    Assert.IsTrue(people != null && string.IsNullOrEmpty(error) && people != null && result && id_3 != people.ID && id_3 != id_2);
                }
                else
                {
                    Assert.Fail("Can't add new people");
                }
            }
            else
            {
                Assert.Fail(string.Concat("Can't add new people ", error));
            }
        }
    }
}
