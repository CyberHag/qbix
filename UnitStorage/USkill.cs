﻿using InterfaceObjectStorage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Storage;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitStorage
{
    [TestClass]
    public class USkill
    {
        [TestMethod]
        public void TestAddandDelete()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            string error = string.Empty;
            int id_1 = 0;
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select max([id]) from [skill]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id_1 = reader.GetInt32(0);
                    }
                }
            }
            ISkill skill_new = new SkillR(storage)
            {
                Name = string.Concat("Test_Add_Skill", DateTime.Now.ToShortTimeString())
            };
            if (skill_new.Save(out error))
            {
                int id_2 = 0;
                using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
                {
                    using (SqlCommand command = new SqlCommand("select max([id]) from [skill]", connection))
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            id_2 = reader.GetInt32(0);
                        }
                    }
                }
                if (id_1 != id_2)
                {
                    ISkill skill = storage.LoadSkill(skill_new.ID, out error);
                    bool result = skill.Delete(out error);
                    int id_3 = 0;
                    using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
                    {
                        using (SqlCommand command = new SqlCommand("select max([id]) from [skill]", connection))
                        {
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.Read())
                            {
                                id_3 = reader.GetInt32(0);
                            }
                        }
                    }
                    Assert.IsTrue(skill != null && string.IsNullOrEmpty(error) && skill != null && result && id_3 != skill.ID && id_3 != id_2);
                }
                else
                {
                    Assert.Fail("Can't add new skill");
                }
            }
            else
            {
                Assert.Fail(string.Concat("Can't add new skill ", error));
            }
        }

        [TestMethod]
        public void TestUpdate()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            int id = 0;
            string error = string.Empty;
            using (SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using (SqlCommand command = new SqlCommand("select max([id]) from [skill]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id = reader.GetInt32(0);
                    }
                }
            }
            ISkill skill = storage.LoadSkill(id, out error);
            if (skill != null && string.IsNullOrEmpty(error))
            {
                string name_true = skill.Name;
                skill.Name = string.Concat(skill.Name, DateTime.Now.ToShortTimeString());
                Assert.IsTrue(skill.Update(out error) && string.IsNullOrEmpty(error) && skill.Name.Equals(name_true));
            }
            else
            {
                Assert.Fail(string.Concat("Can't load skill", error));
            }
        }

        [TestMethod]
        public void TestSave()
        {
            Storage.Storage storage = new Storage.Storage();
            storage.ConnectionString = Global.GetConnectionString();

            int id = 0;
            string error = string.Empty;
            using(SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
            {
                using(SqlCommand command = new SqlCommand("select max([id]) from [skill]", connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        id = reader.GetInt32(0);
                    }
                }
            }
            ISkill skill = storage.LoadSkill(id, out error);
            if (skill != null) {
                string name = skill.Name;
                string name_date = DateTime.Now.ToShortTimeString();
                if(name.Length > name_date.Length + 1)
                {
                    skill.Name = string.Concat(name.Substring(0, name.Length - name_date.Length - 1), " ", name_date);
                }
                else
                {
                    skill.Name = string.Concat(name, " ", name_date);
                }
                if(skill.Save(out error) && string.IsNullOrEmpty(error))
                {
                    using(SqlConnection connection = new SqlConnection(Global.GetConnectionString()))
                    {
                        using(SqlCommand command = new SqlCommand(string.Concat("select * from [skill] where [id] = ", id), connection))
                        {
                            connection.Open();
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.Read())
                            {
                                string name_new = reader.GetString(reader.GetOrdinal("name"));
                                Assert.IsTrue(name_new.Equals(skill.Name));
                            }
                            else
                            {
                                Assert.Fail("Can't load skill");
                            }
                        }
                    }
                }
                else
                {
                    Assert.Fail(error);
                }
            }
            else
            {
                Assert.Fail("Can't load skill");
            }
        }
    }
}
