﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceObjectStorage
{
    /// <summary>
    /// Уровень владения навыком
    /// Не имеет практического значения без привязки к конкретному человеку
    /// </summary>
    public interface ISkillLevel 
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        int ID { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Уровень владения навыком
        /// </summary>
        int Level { get; set; }
        /// <summary>
    }
}
