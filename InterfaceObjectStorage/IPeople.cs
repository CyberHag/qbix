﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceObjectStorage
{
    /// <summary>
    /// Человек
    /// </summary>
    public interface IPeople
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        int ID { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Должность
        /// </summary>
        IPosition Positions { get; set; }
        /// <summary>
        /// Список навыков
        /// </summary>
        IEnumerable<ISkillLevel> Skills { get; set; }
        /// <summary>
        /// Метод сохранения в хранилище
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Флаг успешности сохранения</returns>
        bool Save(out string Error);
        /// <summary>
        /// Метод обновления из хранилища
        /// дозагружает, если не было ранее данные о навыках
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Флаг успешности обновления</returns>
        bool Update(out string Error);
        /// <summary>
        /// Метод удаления в хранилище
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Флаг успешности удаления</returns>
        bool Delete(out string Error);
    }
}
