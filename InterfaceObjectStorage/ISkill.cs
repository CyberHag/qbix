﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceObjectStorage
{
    /// <summary>
    /// Навык
    /// </summary>
    public interface ISkill
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        int ID { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Метод сохранения в хранилище
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <param name="NeedUpdate">Флаг уведомляющий об несоответствии версии</param>
        /// <returns>Флаг успешности сохранения</returns>
        bool Save(out string Error);
        /// <summary>
        /// Метод обновления из хранилища
        /// дозагружает, если не было ранее данные о навыках
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Флаг успешности обновления</returns>
        bool Update(out string Error);
        /// <summary>
        /// Метод удаления в хранилище
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Флаг успешности удаления</returns>
        bool Delete(out string Error);
    }
}
