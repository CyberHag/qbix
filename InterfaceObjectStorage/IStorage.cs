﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceObjectStorage
{
    /// <summary>
    /// Функции связи с хранилищем данных
    /// </summary>
    public interface IStorage
    {
        /// <summary>
        /// Строка соединения
        /// </summary>
        string ConnectionString { get; set; }
        /// <summary>
        /// Загрузка данных о человеке
        /// </summary>
        /// <param name="ID">Идентификатор человека</param>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Человек</returns>
        IPeople LoadPeople(int ID, out string Error);
        /// <summary>
        /// Загрузка данных о должности
        /// </summary>
        /// <param name="ID">Идентификатор должности</param>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Должность</returns>
        IPosition LoadPosition(int ID, out string Error);
        /// <summary>
        /// Загрузка данных о навыке
        /// </summary>
        /// <param name="ID">Идентификатор навыка</param>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Навык</returns>
        ISkill LoadSkill(int ID, out string Error);
        /// <summary>
        /// Загрузка множества людей
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Множество людей</returns>
        IEnumerable<IPeople> LoadEnumerablePeople(out string Error);
        /// <summary>
        /// Загрузка множества должностей
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Множество должностей</returns>
        IEnumerable<IPosition> LoadEnumerablePosition(out string Error);
        /// <summary>
        /// Загрузка множества навыков
        /// </summary>
        /// <param name="Error">Сообщение об ошибке</param>
        /// <returns>Множество навыков</returns>
        IEnumerable<ISkill> LoadEnumerableSkill(out string Error);
        /// <summary>
        /// Возвращает пустой объект сотрудника
        /// </summary>
        /// <returns></returns>
        IPeople GetEmptyPeople();
        /// <summary>
        /// Возвращает пустой объект должности
        /// </summary>
        /// <returns></returns>
        IPosition GetEmptyPosition();
        /// <summary>
        /// Возвращает пустой объект навыка
        /// </summary>
        /// <returns></returns>
        ISkill GetEmptySkill();
        /// <summary>
        /// Возвращает пустой объект уровня владения навыком
        /// </summary>
        /// <returns></returns>
        ISkillLevel GetEmptySkillLevel();
    }
}
