﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;

namespace Storage
{
    [Table(Name="skill")]
    class DBSkill
    {
        [Column(IsPrimaryKey =true, IsDbGenerated = true, Name = "id")]
        public int ID { get; set; }
        [Column(Name = "name")]
        public string Name { get; set; }
    }
}
