﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;

namespace Storage
{
    [Table(Name = "people")]
    class DBPeople
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, Name = "id")]
        public int ID { get; set; }
        [Column(Name = "name")]
        public string Name { get; set; }
        [Column(Name = "position_id")]
        public int PositionID { get; set; }
    }
}
