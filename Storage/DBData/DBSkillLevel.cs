﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;

namespace Storage
{
    [Table(Name = "skill_level")]
    class DBSkillLevel
    {
        [Column(Name = "people_id")]
        public int PeopleID { get; set; }
        [Column(Name = "skill_id")]
        public int SkillID { get; set; }
        [Column(Name = "level")]
        public int Level { get; set; }
        [Column(Name = "id", IsPrimaryKey = true, IsDbGenerated = true)]
        protected int ID { get; set; }
    }
}
