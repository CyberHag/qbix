﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;

namespace Storage
{
    [Table(Name = "position_skill")]
    class DBPositionSkill
    {
        [Column(Name = "position_id")]
        public int PositionID { get; set; }
        [Column(Name = "skill_id")]
        public int SkillID { get; set; }
        [Column(Name = "id", IsPrimaryKey = true, IsDbGenerated = true)]
        public int ID { get; set; }
    }
}
