﻿using InterfaceObjectStorage;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace Storage
{
    public class PeopleR : IPeople
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public IPosition Positions { get; set; }
        public IEnumerable<ISkillLevel> Skills { get; set; }
        protected IStorage storage;

        public PeopleR(IStorage Storage)
        {
            storage = Storage;
            Positions = new PositionR(storage);
            Skills = new List<ISkillLevel>();
        }
        private PeopleR()
        {
            Skills = new List<ISkillLevel>();
        }

        public bool Delete(out string Error)
        {
            Error = string.Empty;
            DataContext dataContext = null;
            try
            {
                dataContext = new DataContext(storage.ConnectionString);
                dataContext.Connection.Open();
                dataContext.Transaction = dataContext.Connection.BeginTransaction();
                dataContext.ExecuteCommand("delete from [skill_level] where [people_id] = {0}", ID);
                if (dataContext.ExecuteCommand("delete from [people] where [id] = {0}", ID) > 0)
                {
                    dataContext.Transaction.Commit();
                    return true;
                }
                else
                {
                    dataContext.Transaction.Rollback();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                if (dataContext != null && dataContext.Transaction != null)
                {
                    dataContext.Transaction.Rollback();
                }
                return false;
            }
        }

        public bool Save(out string Error)
        {
            Error = string.Empty;
            DataContext dataContext = null;
            try
            {
                dataContext = new DataContext(storage.ConnectionString);
                if (ID == 0)
                {
                    dataContext.Connection.Open();
                    dataContext.Transaction = dataContext.Connection.BeginTransaction();
                    DBPeople people = new DBPeople()
                    {
                        Name = Name,
                        PositionID = Positions.ID
                    };
                    dataContext.GetTable<DBPeople>().InsertOnSubmit(people);
                    dataContext.SubmitChanges();
                    if (people.ID > 0)
                    {
                        ID = people.ID;
                        if (Skills != null)
                        {
                            foreach (ISkillLevel oneSkill in Skills)
                            {
                                DBSkillLevel skill = new DBSkillLevel()
                                {
                                    SkillID = oneSkill.ID,
                                    PeopleID = ID,
                                    Level = oneSkill.Level
                                };
                                dataContext.GetTable<DBSkillLevel>().InsertOnSubmit(skill);
                            }
                            dataContext.SubmitChanges();
                        }
                        dataContext.Transaction.Commit();
                        return true;
                    }
                    else
                    {
                        dataContext.Transaction.Rollback();
                        Error = "Can't add new people";
                        return false;
                    }
                }
                else
                {
                    dataContext.Connection.Open();
                    dataContext.Transaction = dataContext.Connection.BeginTransaction();
                    var people = dataContext.GetTable<DBPeople>().Single(p => p.ID == ID);
                    if (people != null)
                    {
                        people.Name = Name;
                        people.PositionID = Positions.ID;
                        dataContext.SubmitChanges();
                        if (Skills != null)
                        {
                            dataContext.ExecuteCommand("delete from [skill_level] where [people_id] = {0}", ID);
                            foreach (ISkillLevel oneSkill in Skills)
                            {
                                DBSkillLevel skill = new DBSkillLevel()
                                {
                                    SkillID = oneSkill.ID,
                                    PeopleID = ID,
                                    Level = oneSkill.Level
                                };
                                dataContext.GetTable<DBSkillLevel>().InsertOnSubmit(skill);
                                dataContext.SubmitChanges();
                            }
                        }
                        dataContext.Transaction.Commit();
                        return true;
                    }
                    else
                    {
                        Error = "Can't load people";
                        dataContext.Transaction.Rollback();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                if (dataContext != null && dataContext.Transaction != null)
                {
                    dataContext.Transaction.Rollback();
                }
                return false;
            }
        }

        public bool Update(out string Error)
        {
            Error = string.Empty;
            try
            {
                DataContext dataContext = new DataContext(storage.ConnectionString);
                var people = (from peo in dataContext.GetTable<DBPeople>()
                              join pos in dataContext.GetTable<DBPosition>()
                              on peo.PositionID equals pos.ID into peo_pos
                              where peo.ID == ID
                              from item in peo_pos.DefaultIfEmpty()
                              select new { peo.ID, peo.Name, PositionID = item == null ? 0 : item.ID, PositionName = item == null ? string.Empty : item.Name }).FirstOrDefault();
                if (people != null)
                {
                    Name = people.Name;
                    Positions = new PositionR(storage)
                    {
                        ID = people.PositionID,
                        Name = people.PositionName
                    };
                    var levels = from ski in dataContext.GetTable<DBSkill>()
                                 join pos_ski in dataContext.GetTable<DBPositionSkill>()
                                 on ski.ID equals pos_ski.SkillID
                                 where pos_ski.PositionID == Positions.ID
                                 join lev in dataContext.GetTable<DBSkillLevel>()
                                 on new { SID = ski.ID, PID = ID } equals new { SID = lev.SkillID, PID = lev.PeopleID } into ski_lev
                                 from item in ski_lev.DefaultIfEmpty()
                                 select new { ski.ID, ski.Name, Level = item == null ? 0 : item.Level };
                    // в идеале очистить множество и заполнить заново, но для тестовой задачи упростим
                    Skills = new List<ISkillLevel>();
                    foreach (var oneLevel in levels)
                    {
                        ISkillLevel skill = new SkillLevelR(storage)
                        {
                            ID = oneLevel.ID,
                            Name = oneLevel.Name,
                            Level = oneLevel.Level
                        };
                        (Skills as List<ISkillLevel>).Add(skill);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
    }
}
