﻿using InterfaceObjectStorage;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace Storage
{
    public class SkillLevelR : ISkillLevel
    {
        public int Level { get; set; }
        public int ID { get ; set ; }
        public string Name { get ; set ; }

        protected IStorage storage;

        public SkillLevelR(IStorage Storage) { storage = Storage; }
    }
}
