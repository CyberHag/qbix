﻿using InterfaceObjectStorage;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace Storage
{
    public class PositionR : IPosition
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public IEnumerable<ISkill> Skills { get; set; }
        protected IStorage storage;

        public PositionR(IStorage Storage)
        {
            storage = Storage;
        }

        private PositionR() { Skills = new List<ISkill>(); }

        public bool Delete(out string Error)
        {
            Error = string.Empty;
            DataContext dataContext = null;
            try
            {
                dataContext = new DataContext(storage.ConnectionString);
                dataContext.Connection.Open();
                dataContext.Transaction = dataContext.Connection.BeginTransaction();
                dataContext.ExecuteCommand("delete from [position_skill] where [position_id] = {0}", ID);
                if (dataContext.ExecuteCommand("delete from [position] where [id] = {0}", ID) > 0)
                {
                    dataContext.Transaction.Commit();
                    return true;
                }
                else
                {
                    dataContext.Transaction.Rollback();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                if (dataContext != null && dataContext.Transaction != null)
                {
                    dataContext.Transaction.Rollback();
                }
                return false;
            }
        }

        public bool Save(out string Error)
        {
            Error = string.Empty;
            DataContext dataContext = null;
            try
            {
                dataContext = new DataContext(storage.ConnectionString);
                if (ID == 0)
                {
                    dataContext.Connection.Open();
                    dataContext.Transaction = dataContext.Connection.BeginTransaction();
                    DBPosition position = new DBPosition()
                    {
                        Name = Name
                    };
                    dataContext.GetTable<DBPosition>().InsertOnSubmit(position);
                    dataContext.SubmitChanges();
                    if (position.ID > 0)
                    {
                        ID = position.ID;
                        if (Skills != null)
                        {
                            foreach (ISkill oneSkill in Skills)
                            {
                                DBPositionSkill skill = new DBPositionSkill()
                                {
                                    PositionID = ID,
                                    SkillID = oneSkill.ID
                                };
                                dataContext.GetTable<DBPositionSkill>().InsertOnSubmit(skill);
                            }
                            dataContext.SubmitChanges();
                        }
                        dataContext.Transaction.Commit();
                        return true;
                    }
                    else
                    {
                        dataContext.Transaction.Rollback();
                        Error = "Can't add new position";
                        return false;
                    }
                }
                else
                {
                    dataContext.Connection.Open();
                    dataContext.Transaction = dataContext.Connection.BeginTransaction();
                    var position = dataContext.GetTable<DBPosition>().Single(p => p.ID == ID);
                    if(position != null)
                    {
                        position.Name = Name;
                        dataContext.SubmitChanges();
                        if (Skills != null)
                        {
                            dataContext.ExecuteCommand("delete from [position_skill] where [position_id] = {0}", ID);
                            foreach(ISkill oneSkill in Skills)
                            {
                                DBPositionSkill skill = new DBPositionSkill()
                                {
                                    PositionID = ID,
                                    SkillID = oneSkill.ID
                                };
                                dataContext.GetTable<DBPositionSkill>().InsertOnSubmit(skill);
                                dataContext.SubmitChanges();
                            }
                        }
                        dataContext.Transaction.Commit();
                        return true;
                    }
                    else
                    {
                        Error = "Can't load position";
                        dataContext.Transaction.Rollback();
                        return false;
                    }
                }
            }catch(Exception ex)
            {
                Error = ex.Message;
                if (dataContext != null && dataContext.Transaction != null)
                {
                    dataContext.Transaction.Rollback();
                }
                return false;
            }
        }

        public bool Update(out string Error)
        {
            Error = string.Empty;
            try
            {
                DataContext dataContext = new DataContext(storage.ConnectionString);
                var position = dataContext.GetTable<DBPosition>().Single(p => p.ID == ID);
                if (position != null)
                {
                    Name = position.Name;
                    var skills = from pos in dataContext.GetTable<DBPositionSkill>()
                                 join skill in dataContext.GetTable<DBSkill>() on pos.SkillID equals skill.ID
                                 where pos.PositionID == ID
                                 select skill;
                    // в идеале очистить множество и заполнить заново, но для тестовой задачи упростим
                    Skills = new List<ISkill>();
                    foreach (var oneSkill in skills)
                    {
                        ISkill skill = new SkillR(storage)
                        {
                            ID = oneSkill.ID,
                            Name = oneSkill.Name
                        };
                        (Skills as List<ISkill>).Add(skill);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
