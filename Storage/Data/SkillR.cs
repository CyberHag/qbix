﻿using InterfaceObjectStorage;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;

namespace Storage
{
    public class SkillR : ISkill
    {
        public int ID { get; set; }
        public string Name { get; set; }
        protected IStorage storage;

        public SkillR(IStorage Storage)
        {
            storage = Storage;
        }

        private SkillR() { }

        public bool Delete(out string Error)
        {
            Error = string.Empty;
            try
            {
                DataContext dataContext = new DataContext(storage.ConnectionString);

                return dataContext.ExecuteCommand("delete from [skill] where [id] = {0}", ID) > 0;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool Save(out string Error)
        {
            Error = string.Empty;
            try
            {
                DataContext dataContext = new DataContext(storage.ConnectionString);

                if (ID > 0)
                {
                    var skill = dataContext.GetTable<DBSkill>().Single(s => s.ID == ID);
                    if(skill != null)
                    {
                        skill.Name = Name;
                        dataContext.SubmitChanges();
                        return true;
                    }
                    else
                    {
                        Error = "Can't find skill with ID";
                        return false;
                    }
                }
                else
                {
                    var skill = new DBSkill()
                    {
                        Name = Name
                    };
                    dataContext.GetTable<DBSkill>().InsertOnSubmit(skill);
                    dataContext.SubmitChanges();
                    ID = skill.ID;
                    return true;
                }
            }catch(Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool Update(out string Error)
        {
            Error = string.Empty;
            try
            {
                DataContext dataContext = new DataContext(storage.ConnectionString);
                var skill = dataContext.GetTable<DBSkill>().Single(s => s.ID == ID);
                if(skill != null)
                {
                    Name = skill.Name;
                    return true;
                }
                else
                {
                    Error = "Can't load skill by ID";
                    return false;
                }
            }catch(Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
    }
}
