﻿using InterfaceObjectStorage;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace Storage
{
    public class Storage : InterfaceObjectStorage.IStorage
    {
        public string ConnectionString { get; set; }

        public IEnumerable<IPeople> LoadEnumerablePeople(out string Error)
        {
            Error = string.Empty;
            List<IPeople> peoples = new List<IPeople>();
            try
            {
                DataContext dataContext = new DataContext(ConnectionString);
                var TablePeople = from people in dataContext.GetTable<DBPeople>()
                                  join position in dataContext.GetTable<DBPosition>()
                                  on people.PositionID equals position.ID into peoplePosition
                                  from item in peoplePosition.DefaultIfEmpty()
                                  let posID = item == null ? 0 : item.ID
                                  let posName = item == null ? string.Empty : item.Name
                                  select new { ID = people.ID, Name = people.Name, PositionID = posID, Position = posName };
                foreach (var people in TablePeople)
                {
                    PeopleR onePeople = new PeopleR(this)
                    {
                        ID = people.ID,
                        Name = people.Name,
                        Positions = new PositionR(this)
                        {
                            ID = people.PositionID,
                            Name = people.Position
                        }
                    };
                    peoples.Add(onePeople);
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
            return peoples;
        }

        public IEnumerable<IPosition> LoadEnumerablePosition(out string Error)
        {
            Error = string.Empty;
            List<IPosition> positions = new List<IPosition>();
            try
            {
                DataContext dataContext = new DataContext(ConnectionString);
                var TablePosition = dataContext.GetTable<DBPosition>();
                foreach (var position in TablePosition)
                {
                    PositionR onePosition = new PositionR(this)
                    {
                        ID = position.ID,
                        Name = position.Name
                    };
                    positions.Add(onePosition);
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
            return positions;
        }

        public IEnumerable<ISkill> LoadEnumerableSkill(out string Error)
        {
            Error = string.Empty;
            List<ISkill> skills = new List<ISkill>();
            try
            {
                DataContext dataContext = new DataContext(ConnectionString);
                var TableSkill = dataContext.GetTable<DBSkill>();
                foreach (var skill in TableSkill)
                {
                    SkillR oneSkill = new SkillR(this)
                    {
                        ID = skill.ID,
                        Name = skill.Name
                    };
                    skills.Add(oneSkill);
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
            return skills;
        }

        public IPeople LoadPeople(int ID, out string Error)
        {
            Error = string.Empty;
            PeopleR result = null;
            try
            {
                DataContext dataContext = new DataContext(ConnectionString);
                var SinglePeople = (from people in dataContext.GetTable<DBPeople>()
                                    join position in dataContext.GetTable<DBPosition>()
                                    on people.PositionID equals position.ID into peoplePosition
                                    where people.ID == ID
                                    from item in peoplePosition.DefaultIfEmpty()
                                    let posID = item == null ? 0 : item.ID
                                    let posName = item == null ? string.Empty : item.Name
                                    select new { ID = people.ID, Name = people.Name, PositionID = posID, Position = posName }).FirstOrDefault();
                if (SinglePeople != null)
                {
                    result = new PeopleR(this)
                    {
                        ID = SinglePeople.ID,
                        Name = SinglePeople.Name,
                        Positions = new PositionR(this)
                        {
                            ID = SinglePeople.PositionID,
                            Name = SinglePeople.Position,
                            Skills = new List<ISkill>()
                        },
                        Skills = new List<ISkillLevel>()
                    };
                    var TableSkill = from positionSkill in dataContext.GetTable<DBPositionSkill>()
                                     join skill in dataContext.GetTable<DBSkill>()
                                     on positionSkill.SkillID equals skill.ID into positionSkillList
                                     where positionSkill.PositionID == SinglePeople.PositionID
                                     from item in positionSkillList
                                     let skiID = item == null ? 0 : item.ID
                                     let skiName = item == null ? string.Empty : item.Name
                                     select new { ID = skiID, Name = skiName };
                    foreach (var oneSkill in TableSkill)
                    {
                        SkillR skill = new SkillR(this)
                        {
                            ID = oneSkill.ID,
                            Name = oneSkill.Name
                        };
                        (result.Positions.Skills as List<ISkill>).Add(skill);
                        SkillLevelR skillLevel = new SkillLevelR(this)
                        {
                            ID = oneSkill.ID,
                            Name = oneSkill.Name,
                            Level = 0
                        };
                        (result.Skills as List<ISkillLevel>).Add(skillLevel);
                    }
                    var TableSkillLevel = dataContext.GetTable<DBSkillLevel>().Where(l => l.PeopleID == ID);
                    foreach (var oneSkillLevel in TableSkillLevel)
                    {
                        var skillLevel = result.Skills.Single(l => l.ID == oneSkillLevel.SkillID);
                        if (skillLevel != null)
                        {
                            skillLevel.Level = oneSkillLevel.Level;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
            return result;
        }

        public IPosition LoadPosition(int ID, out string Error)
        {
            Error = string.Empty;
            PositionR result = null;
            try
            {
                DataContext dataContext = new DataContext(ConnectionString);
                var SinglePosition = dataContext.GetTable<DBPosition>().Single(p => p.ID == ID);
                if (SinglePosition != null)
                {
                    result = new PositionR(this)
                    {
                        ID = SinglePosition.ID,
                        Name = SinglePosition.Name,
                        Skills = new List<ISkill>()
                    };
                    var TableSkill = from positionSkill in dataContext.GetTable<DBPositionSkill>()
                                     join skill in dataContext.GetTable<DBSkill>()
                                     on positionSkill.SkillID equals skill.ID into positionSkillList
                                     where positionSkill.PositionID == SinglePosition.ID
                                     from item in positionSkillList
                                     let skiID = item == null ? 0 : item.ID
                                     let skiName = item == null ? string.Empty : item.Name
                                     select new { ID = skiID, Name = skiName };
                    foreach (var oneSkill in TableSkill)
                    {
                        SkillR skill = new SkillR(this)
                        {
                            ID = oneSkill.ID,
                            Name = oneSkill.Name
                        };
                        (result.Skills as List<ISkill>).Add(skill);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
            return result;
        }

        public ISkill LoadSkill(int ID, out string Error)
        {
            Error = string.Empty;
            SkillR result = null;
            try
            {
                DataContext dataContext = new DataContext(ConnectionString);
                var skill = dataContext.GetTable<DBSkill>().Single(s => s.ID == ID);
                if(skill != null)
                {
                    result = new SkillR(this)
                    {
                        ID = skill.ID,
                        Name = skill.Name
                    };
                }
            }catch(Exception ex)
            {
                Error = ex.Message;
            }
            return result;
        }

        public IPeople GetEmptyPeople()
        {
            return new PeopleR(this);
        }

        public IPosition GetEmptyPosition()
        {
            return new PositionR(this);
        }

        public ISkill GetEmptySkill()
        {
            return new SkillR(this);
        }

        public ISkillLevel GetEmptySkillLevel()
        {
            return new SkillLevelR(this);
        }
    }
}
